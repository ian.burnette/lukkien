"""lukkien URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from lukkien.contacts.views import contact_us, sign_up
from lukkien.contacts.api import api_paths
from lukkien.contacts.demo import populate, reset

urlpatterns = [
    path('admin/', admin.site.urls),
    path('contact_us/', contact_us),
    path('sign_up/', sign_up),
] + api_paths

# Don't let users delete the prod database...
if settings.DEBUG:
    urlpatterns += [
        path('demo/populate/<int:num>/', populate),
        path('demo/reset/', reset),
    ]
