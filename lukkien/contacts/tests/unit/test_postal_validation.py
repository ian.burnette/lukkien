import pytest
from django.core.exceptions import ValidationError

from lukkien.contacts.validators import is_postal_code, Errors
from lukkien.contacts.tests.unit.util import _run_validation_test


@pytest.mark.parametrize("val, error", [
    # Lukkien (hopefully valid!)
    ("6716BM", None),
    # SS, SA, SD are not allowed due to the Nazi occupation
    ("6666SA", ValidationError(Errors.NAZI_ERROR)),
    ("6666SD", ValidationError(Errors.NAZI_ERROR)),
    ("6666SS", ValidationError(Errors.NAZI_ERROR)),
])
def test_dutch_matcher(val, error):
    """
    Tests for exceptions that should be caught in DutchPostalMatcher.__call__

    Also tests to ensure that valid Dutch codes pass our validation check
    """
    _run_validation_test(is_postal_code, val, error)


@pytest.mark.parametrize("val, error", [
    # No Trumps allowed
    ("20500", ValidationError(Errors.TRUMP_ERROR)),
    # Valid
    ("29414", None)
])
def test_us_zip_matcher(val, error):
    """
    Tests for exceptions that should be caught in USPostalMatcher.__call__

    Also tests to ensure that valid ZIP codes pass our validation check
    """
    _run_validation_test(is_postal_code, val, error)


@pytest.mark.parametrize("val, error", [
    # The space should've been cleared out by clean_data
    ("6716 BM", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    # Likewise, mixed and lower case letters should be capitalized
    ("6716 bM", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    ("6716bm", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    # Empty string
    ("", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    # Too short
    ("1234", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    ("123AA", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    ("1234A", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    # Too long
    ("123456", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    ("12345AA", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    ("1234AAA", ValidationError(Errors.DEFAULT_POSTAL_ERROR)),
    # Must be string
    (12345, TypeError()),
    (["1234AA"], TypeError()),
    ([1, 2, 3, 4, 5], TypeError()),
])
def test_postal_matcher(val, error):
    """
    Tests for exceptions that should be caught in PostalMatcher.__call__
    """
    _run_validation_test(is_postal_code, val, error)
