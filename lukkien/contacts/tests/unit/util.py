import pytest


def _run_validation_test(func, val, error=None):
    """
    Runs a test on a validator function. If `error` is provided we check that
    an error of the appropriate type and message is called. Otherwise we call
    `func(val)` and expect it to return without raising anything.

    :param func: The validation callable to be tested
    :param val: The value we are validating
    :param error: The error we're expecting (or None)
    """
    if error:
        match = None
        if hasattr(error, "message"):
            match = error.message
        with pytest.raises(type(error), match=match):
            func(val)
    else:
        func(val)
