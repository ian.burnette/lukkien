import pytest
from django.core.exceptions import ValidationError

from lukkien.contacts.validators import is_phone_number, Errors
from lukkien.contacts.tests.unit.util import _run_validation_test


@pytest.mark.parametrize("val, error", [
    # 0 prefix should not be included with country code
    ("310655555555", ValidationError(Errors.DUTCH_PREFIX_ERROR)),
    # pager numbers (66) are not valid
    ("31665555555", ValidationError(Errors.PAGER_ERROR)),
    # Too many digits for a dutch number
    ("316555555555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Toll free numbers (800) are not valid
    ("31800555555", ValidationError(Errors.TOLL_FREE_ERROR)),
    # Should be valid... at least according to wikipedia!
    ("31655555555", None),
])
def test_dutch_matcher(val, error):
    """
    Tests for exceptions that should be caught in DutchPhoneMatcher.__call__

    Also tests to ensure that valid Dutch numbers pass our validation check
    """
    _run_validation_test(is_phone_number, val, error)


@pytest.mark.parametrize("val, error", [
    # Toll free numbers (800) are not valid
    ("18005555555", ValidationError(Errors.TOLL_FREE_ERROR)),
    # Toll free numbers (866) are not valid
    ("18665555555", ValidationError(Errors.TOLL_FREE_ERROR)),
    # 860 and 806 should not be caught as toll free numbers
    ("18065555555", None),
    ("18605555555", None),
    # tests an actual area code
    ("18435555555", None),
])
def test_nanp_matcher(val, error):
    """
    Tests for exceptions that should be caught in NANPPhoneMatcher.__call__

    Also tests to ensure that valid NANP numbers pass our validation check
    """
    _run_validation_test(is_phone_number, val, error)


@pytest.mark.parametrize("val, error", [
    # Empty string isn't valid
    ("", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Too short
    ("1843555555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # WAY too short
    ("1", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Too long
    ("184355555555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Contains letters (NANP prefix)
    ("1AAABBBBBBB", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Contains letters (Netherlands prefix)
    ("31AABBBBBBB", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Completely wrong type of string
    ("Hello World", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Phone numbers fail if common delimiters haven't been stripped
    ("1 843 555 5555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    ("1 (843) 555-5555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    ("1.843.555.5555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    ("1-843-555-5555", ValidationError(Errors.DEFAULT_PHONE_ERROR)),
    # Should be a str
    (11234567890, TypeError()),
    (['1', '8', '4', '3', '5', '5', '5', '5', '5', '5', '5'], TypeError()),
])
def test_matcher(val, error):
    """
    Tests for exceptions that should be caught by within PhoneValidator.__call__
    """
    _run_validation_test(is_phone_number, val, error)
