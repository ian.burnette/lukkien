from django.http import JsonResponse
from django.urls import path
from django.forms.models import model_to_dict
from django.db.models import Count

from lukkien.contacts.models import Contact


VERSION = 1
BASE_URL = 'api/{0}'.format(VERSION)


def get_contacts(request, **kwargs):
    fields = [field.name for field in Contact._meta.fields]
    url_field = next(iter([f for f in fields if f in request.path]), None)
    kwargs_field = next(iter([f for f in fields if [f in key for key in kwargs]]), None)
    if url_field and not kwargs_field:
        def _make_url(x):
            return "http://{0}/{1}/{2}/{3}/".format(request.get_host(),
                                                    BASE_URL,
                                                    url_field,
                                                    x)
        db_lookup = Contact.objects.values(url_field).annotate(count=Count("email"))
        data = [d for d in db_lookup if not d.update({'url': _make_url(d[url_field])})]
    else:
        db_lookup = Contact.objects.filter(**kwargs)
        data = [model_to_dict(d, fields=fields) for d in db_lookup]
    return JsonResponse(data, safe=False)


api_paths = [
    path(BASE_URL + '/contacts/', get_contacts),
    path(BASE_URL + '/contacts/<email>/', get_contacts),
    path(BASE_URL + '/postal_code/', get_contacts),
    path(BASE_URL + '/postal_code/<postal_code>/', get_contacts),
    path(BASE_URL + '/city/', get_contacts),
    path(BASE_URL + '/city/<city>/', get_contacts),
    path(BASE_URL + '/phone_number/<phone_number__startswith>/', get_contacts)
]

