from django.db import models
from lukkien.contacts.validators import is_phone_number, is_postal_code


class Contact(models.Model):
    email = models.EmailField(primary_key=True, max_length=100)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=14,
                                    validators=[is_phone_number])
    date_of_birth = models.DateField(blank=True, null=True)
    postal_code = models.CharField(max_length=6, blank=True,
                                   validators=[is_postal_code])
    city = models.CharField(max_length=50, blank=True)
    street = models.CharField(max_length=50, blank=True)
    house_number = models.PositiveIntegerField(blank=True, null=True)
