from django.core.exceptions import ValidationError
import re


class Errors:
    # Phone errors
    DEFAULT_PHONE_ERROR = "Please enter a valid phone number."
    TOLL_FREE_ERROR = "Toll free numbers are not allowed."
    PAGER_ERROR = "Pager numbers are not allowed."
    DUTCH_PREFIX_ERROR = "Please do not include the 0 prefix."

    # Postal Code errors
    DEFAULT_POSTAL_ERROR = "Please enter a valid postal code."
    NAZI_ERROR = "Nazis are bad; please enter a valid postal code."
    TRUMP_ERROR = "No thanks, Mr. President"


class Matcher:
    index = {}

    def __init__(self, value=None):
        pass

    @classmethod
    def default(cls, value):
        if hasattr(cls, "default_error"):
            raise ValidationError(cls.default_error)

    @classmethod
    def pre_call_format(cls, value):
        if hasattr(cls, "prefix_length"):
            return value[cls.prefix_length:]
        else:
            return value

    @classmethod
    def __call__(cls, value):
        """
        Traverses cls.index and attempts to match regexs (the keys of cls.index)
        within the value str. Once a match is found we retrieve cls.index[key]
        if it is a string we raise a ValidationError with that string as the
        error message. If cls.index[key] is callable, then we call it with the
        value argument. If cls.index[key] is a type, then we create an object of
        that type prior to running these checks.

        :param value: the field value which is being validated
        :returnL None
        """
        value = cls.pre_call_format(value)
        for pattern, msg_or_matcher in cls.index.items():
            regex = re.compile(pattern)
            match = regex.search(value)
            if match:
                if isinstance(msg_or_matcher, type):
                    # Callable types still need to be instantiated first.
                    msg_or_matcher = msg_or_matcher(value)

                if callable(msg_or_matcher):
                    # Runs additional validation.
                    msg_or_matcher(value)
                    return None
                elif isinstance(msg_or_matcher, str):
                    # Raises ValidationError with the provided message
                    raise ValidationError(msg_or_matcher)
                else:
                    raise TypeError("{0} is not a callable object, "
                                    "function, or str.".format(msg_or_matcher))
        cls.default(value)


class DutchPhoneMatcher(Matcher):
    """
    Validates Dutch phone numbers
    """
    index = {
        "^800": Errors.TOLL_FREE_ERROR,
        "^66": Errors.PAGER_ERROR,
        "^0": Errors.DUTCH_PREFIX_ERROR,
        "^[1-9][0-9]{9}": Errors.DEFAULT_PHONE_ERROR,
    }
    prefix_length = 2


class NANPPhoneMatcher(Matcher):
    """
    Validates US phone numbers
    """
    index = {
        "(^8(?:00|66))": Errors.TOLL_FREE_ERROR,
    }
    prefix_length = 1


class PhoneValidator(Matcher):
    """
    Matches regexs to the appropriate PhoneMatchers
    """
    index = {
        "^31[0-9]{9,10}$": DutchPhoneMatcher,
        "^1[0-9]{10}$": NANPPhoneMatcher,
    }
    default_error = Errors.DEFAULT_PHONE_ERROR


def is_phone_number(value):
    """
    Instantiates and calls a PhoneValidator object.

    This is just syntactic sugar.
    """
    PhoneValidator()(value)


class DutchPostalMatcher(Matcher):
    """
    Validates Dutch postal codes.
    """
    index = {
        # SA, SD, and SS are not allowed due to the Nazi occupation.
        "S[ADS]$": Errors.NAZI_ERROR
    }


class USPostalMatcher(Matcher):
    """
    Validates US zip codes.
    """
    index = {
        # 20500 is unique to the White House
        "^20500$": Errors.TRUMP_ERROR
    }


class PostalValidator(Matcher):
    """
    Matches regexs to the appropriate postal code matchers
    """
    index = {
        "^[0-9]{5}$": USPostalMatcher,
        "^[0-9]{4}[A-Z]{2}$": DutchPostalMatcher,
    }
    default_error = Errors.DEFAULT_POSTAL_ERROR


def is_postal_code(value):
    """
    Instantiates and calls a PostalValidator object.

    This is just syntactic sugar.
    """
    PostalValidator()(value)
