from django.shortcuts import render

from lukkien.contacts.forms import ContactShortForm, ContactLongForm
from lukkien.const import HTTPConsts
from lukkien.contacts.models import Contact


def contact_us(request):
    """
    View for the 'Contact Us' form. If validation is successful then we return
    forward the request to the sign_up view (we don't redirect in order to
    preserve the user's form data).
    """
    if request.method == HTTPConsts.POST:
        form = ContactShortForm(request.POST)
        if form.is_valid():
            Contact.objects.update_or_create(email=form.cleaned_data['email'],
                                             defaults=form.cleaned_data)
            return sign_up(request, follow_up=True)
    else:
        form = ContactShortForm()
    return render(request, 'forms/contact_us.html', {'form': form})


def sign_up(request, follow_up=None):
    """
    View for the 'Sign Up' form. If follow_up==True we render a modified version
    of the form, which partially populates the form with the data we received
    from the 'contact_us' view.

    If validation is successful we render a simple 'thank you' template.
    """
    if request.method == HTTPConsts.POST:
        form = ContactLongForm(request.POST)
        if form.is_valid():
            Contact.objects.update_or_create(email=form.cleaned_data['email'],
                                             defaults=form.cleaned_data)
            return render(request, 'thank_you.html', {})
        elif follow_up:
            return render(request, 'forms/sign_post_contact.html',
             {'form': form,
              'follow_up': True})
    else:
        form = ContactLongForm()

    return render(request, 'forms/sign_up.html', {'form': form})
