from django import forms


def remove(string, chars):
    for char in chars:
        string = string.replace(char, "")
    return string


class PhoneField(forms.CharField):
    def to_python(self, value):
        value = super().to_python(value)
        value = remove(value, "\t\n\b()[]-._+ ")
        return value


class PostalCodeField(forms.CharField):
    def to_python(self, value):
        value = super().to_python(value).upper()
        value = value.replace(" ", "")
        return value
