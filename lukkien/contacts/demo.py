from faker import Faker
import random
from datetime import datetime, timedelta
from django.http import JsonResponse


from lukkien.contacts.models import Contact

NL = "nl_NL"


def nl_phone():
    return "31" + "".join([str(random.randint(0,9)) for i in range(9)])


def us_phone():
    return "1" + "".join([str(random.randint(0,9)) for i in range(10)])


def template(locale):
    fake = Faker(locale)
    cities = [fake.city() for i in range(5)]
    post_codes = [fake.postcode().replace(" ", "") for i in range(20)]
    def _date():
        return fake.date(end_datetime=datetime.today() - timedelta(days=6666))
    data = {
     "email": fake.email,
     "first_name": fake.first_name,
     "last_name": fake.last_name,
     "phone_number": nl_phone if locale == NL else us_phone,
     "date_of_birth": _date,
     "postal_code": post_codes,
     "city": cities,
     "street": fake.street_name,
     "house_number": fake.random_int
    }
    return data


US_TEMPLATE = template("")
NL_TEMPLATE = template(NL)

def make_contact(template):
    data = {}
    for k, v in template.items():
        if callable(v):
            data[k] = v()
        else:
            data[k] = random.choice(v)
    return data


def populate(request, num):
    """
    Exposes a simple script for populating the DB with fake data.
    """
    if num > 250:
        num = 250
    t1 = random.randint(0, num)
    t2 = num - t1
    count = 0
    for t in ([NL_TEMPLATE]*t1 + [US_TEMPLATE]*t2):
        contact = make_contact(t)
        o, created = Contact.objects.update_or_create(email=contact['email'],
                                                      defaults=contact)
        if created:
            count += 1
    return JsonResponse({'count': count})


def reset(request):
    """
    Exposes a simple way to reset the DB.
    """
    a, d = Contact.objects.all().delete()
    return JsonResponse(d)
