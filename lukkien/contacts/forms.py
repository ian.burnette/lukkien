from django import forms
from lukkien.contacts.validators import is_postal_code, is_phone_number
from lukkien.contacts.fields import PhoneField, PostalCodeField


class DateOfBirthInput(forms.widgets.DateInput):
    template_name = "forms/widgets/dob.html"


class PhoneInput(forms.widgets.TextInput):
    template_name = "forms/widgets/phone.html"


class ContactShortForm(forms.Form):
    first_name = forms.CharField(label="First Name", max_length=50)
    last_name = forms.CharField(label="Last Name", max_length=50)
    email = forms.EmailField(max_length=100)
    phone_number = PhoneField(label="Phone Number", max_length=20,
                              validators=[is_phone_number], widget=PhoneInput)
    date_of_birth = forms.DateField(label="Date of Birth",
                                    widget=DateOfBirthInput)


class ContactLongForm(ContactShortForm):
    date_of_birth = None
    house_number = forms.IntegerField(min_value=0)
    street = forms.CharField(max_length=50)
    postal_code = PostalCodeField(max_length=7,
                                   validators=[is_postal_code])
    city = forms.CharField(max_length=50)
