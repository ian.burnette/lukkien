from lukkien.settings import *
import os


# Keep secret key out of any git files
SECRET_KEY = os.environ['SECRET_KEY']

# Disable debug error screens
DEBUG = False

# Import ALLOWED_HOSTS from the environment
ALLOWED_HOSTS = os.environ['ALLOWED_HOSTS']

# Enable various security settings that come with Django
SECURE_BROWSER_XSS_FILTER = True
CSRF_COOKIE_SECURE = True
SECURE_CONTENT_TYPE_NOSNIFF = True
X_FRAME_OPTIONS = 'DENY'
