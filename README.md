# Lukkien Coding Assignment

Hi, my name is Ian Burnette; thank you for your consideration.

### Running the code:
- Clone the repository
- (optional) create a venv.
- Install the requirements:
  - `pip install -r requirements.txt`
- Make/Apply migrations:
  - ```python manage.py makemigrations & python manage.py migrations```
- Run test server:
  - `python manage.py runserver`

## Forms/Views

### Contact Us
The `Contact Us` page (`/contact_us/`) renders a `contacts.forms:ContactShortForm`, 
and requests the following data from the user:
- First Name
- Last Name
- Email
- Phone Number
- Date of Birth

User submissions are validated, and (if clean) the data is used to either create 
or update a new `models:Contact` in the database. The user is then presented
with a modified `Sign Up` page that is prepopulated with the previously validated 
data. Prepopulated fields are purposefully disabled to prevent browsers' autofill
features from modifying the data.


### Sign Up
The `Sign Up` page (`/sign_up/`) renders a `contacts.forms:ContactLongForm`, 
and requests the following data from the user:
- First Name
- Last Name
- Email
- Phone Number
- House Number
- Street
- Postal Code
- City

User submissions are validated, and (if clean) the data is used to either create 
or update a new `models:Contact` in the database. The user is then redirected to
a simple "Thank You" page. Note that the `models:Contact` model is shared between 
both forms, and uses email as it's primary key.

## Production Ready?

I wasn't sure exactly what "Code	should	be	ready	for	release	to	production"
meant in this context. I haven't included things like nginx or postgres
configurations, but I have created a simple `prod_settings.py` file that enables/disables
a few django features that are appropriate for production.

Many security features that I would normally require of a production site are 
not present due to the fact that I am writing this in my free time!

Likewise, I have only included tests for the custom validators I wrote for
this project. I would not deploy a site with so few tests to production, but again,
I do not have much free time.


## Custom Validators

### Phone Number
The Phone Number field is validated using a custom validator that can handle both
US and Dutch phone numbers.

### Postal Code
The Postal Code field is also validated using a custom validator that handles both
US and Dutch postal codes.

### The `Matcher` Class
Both custom validators are based on the callable Matcher class, which allows for
easy organization of various regexs and how they should be grouped/executed.


## API

In addition to the form views, I have built a simple api for accessing saved form
data. All 7 of the api endpoints utilize the same view function. In order to make
the api a bit more interesting, I have also exposed the `/demo/populate/{int < 250}`
endpoint. This endpoint generates up to 250 random entries in the `Contacts` table.
If you'd like to reset the database, you may also use the `/demo/reset/` endpoint
to drop the table. The both `/demo/` endpoints are disabled by default when DEBUG
is disabled in the django settings.

### `/api/1/contacts/`
Returns all `Contacts` as json. Only public fields (i.e. fields defined on the 
model) are included in the results.

### `/api/1/contacts/<email>/`
Returns a list of matched `Contacts` as json, or an empty list. Note that as
email serves as the primary key, this should only ever return a single user.

### `/api/1/postal_code/`
Returns a list of postal codes along with their `Contacts` counts, and their api
url. e.g.:

```
{
  "postal_code": "02783",
  "count": 8,
  "url": "http://localhost:8000/api/1/postal_code/02783/"
}
```

### `/api/1/city/`
Same idea as `/postal_code/`, but with city instead of postal code.

### `/api/1/postal_code/<postal_code>`
Returns a list of `Contacts` living in the given postal code, or an empty list.
Just like with the `/contacts/` endpoint, only the model's explicitly defined
fields are included.

### `/api/1/city/<city>`
Same idea as `/postal_code/`, but with city instead of postal code.

### `/api/1/phone_number/<phone_number__startswith>/`
Returns a list of `Contacts` who's phone numbers match the given prefix. For
example, `/api/1/phone_number/31` would return all the Contacts with a Dutch number.
